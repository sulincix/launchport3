package org.droidtr.launcher3;

public interface OnAlarmListener {
    public void onAlarm(Alarm alarm);
}
